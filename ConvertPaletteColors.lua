local spr = app.activeSprite
local bitMap = {}
bitMap["3 bit"] = 1
bitMap["6 bit"] = 3
bitMap["9 bit"] = 7
bitMap["12 bit"] = 15
bitMap["15 bit"] = 31
bitMap["18 bit"] = 63

local platformToBit = {}
platformToBit["Amiga (OCS/ECS)"] = "12 bit"
platformToBit["Atari 2600 (SECAM)"] = "3 bit"
platformToBit["Atari ST"] = "9 bit"
platformToBit["Atari Lynx"] = "12 bit"
platformToBit["Atari STe"] = "12 bit"
platformToBit["Atari Falcon"] = "18 bit"
platformToBit["NES"] = "6 bit"
platformToBit["SNES"] = "15 bit"
platformToBit["GameBoy Color"] = "15 bit"
platformToBit["GameBoy Advance"] = "15 bit"
platformToBit["Nintendo DS (2D output)"] = "15 bit"
platformToBit["Nintendo DS (3D output and 2D blended output)"] = "18 bit"
platformToBit["SEGA Master System"] = "6 bit"
platformToBit["SEGA Genesis"] = "9 bit"
platformToBit["SEGA Nomad"] = "9 bit"
platformToBit["SEGA Game Gear"] = "12 bit"
platformToBit["Neo Geo Pocket Color"] = "12 bit"
platformToBit["Neo Geo AES/Neo Geo CD"] = "15 bit"

if not spr then
  return app.alert("There is no active sprite")
end

if spr.colorMode ~= ColorMode.INDEXED then
  app.alert("Sprite needs to be indexed")
  return
end

local function round(num)
  return  math.floor(num + 0.5)
end

local function convertValue(colorVal, bitVal)
  local val = round((colorVal * bitVal) / 255)
  local newVal = round((255 / bitVal) * val)
  return newVal
end

local function convertColor(col, choose)
  return Color{ r=convertValue(col.red, bitMap[choose]),
              g=convertValue(col.green, bitMap[choose]),
              b=convertValue(col.blue, bitMap[choose])}
end

local dlg = Dialog()
dlg:combobox{ id="platform",
              label="Choose target platform:",
              option="GameBoy Color",
              options={
                "Amiga (OCS/ECS)",
                "Atari 2600 (SECAM)", 
                "Atari ST", 
                "Atari Lynx",
                "Atari STe",
                "Atari Falcon",
                "NES",
                "SNES",
                "GameBoy Color",
                "GameBoy Advance",
                "Nintendo DS (2D output)",
                "Nintendo DS (3D output and 2D blended output)",
                "SEGA Master System", 
                "SEGA Genesis", 
                "SEGA Nomad", 
                "SEGA Game Gear",
                "Neo Geo Pocket Color",
                "Neo Geo AES/Neo Geo CD"
              },
              onchange=function ()
                local data = dlg.data
                dlg:modify{id="choose", option=platformToBit[data.platform]}
              end
            }
dlg:combobox{ id="choose",
              label="Choose target rgb bit range:",
              option="15 bit",
              options={ "3 bit", "6 bit", "9 bit", "12 bit", "15 bit", "18 bit"}}
dlg:button{ id="ok", text="Convert", onclick=function()
  app.transaction(
    function ()
      local data = dlg.data
      local pal = spr.palettes[1]

      for i = 0,#pal-1 do
        local col = pal:getColor(i)
        pal:setColor(i, convertColor(col, data.choose))
      end
    end)
    
  dlg:close()
  app.refresh()
end }
dlg:button{ id="cancel", text="Cancel" }
dlg:show()
