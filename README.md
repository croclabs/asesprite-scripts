# Asesprite Scripts

Scripts made for use with aseprite

## Palette rgb bit converter

This script is used to convert your palette to either 3, 6, 9, 12, 15 or 18 bit RGB!